var posX = 100;
var posY = 310;

var massBall = 177;
var massCue = 490;
var TROUSHAUT_Y = 6.625;
var TROUSBAS_Y = 120.375;
var TROUSGAUCHE_X = 6.625;
var TROUSDROITE_X = 247.375;
var RAYON_CARRE = 438.9;

//Sliders
var sliderAngle = document.getElementById("orientation");
var outputAngle = document.getElementById("angleOutput");
var sliderSpeed = document.getElementById("speed");
var outputSpeed = document.getElementById("speedOutput");
outputAngle.innerHTML = sliderAngle.value;
outputSpeed.innerHTML = sliderSpeed.value;

sliderAngle.oninput = function() {
  outputAngle.innerHTML = this.value;
}

sliderSpeed.oninput = function(){
  outputSpeed.innerHTML = this.value;
}

function myMove() {
  var elem = document.getElementById("myAnimation"); 
  var id = setInterval(frame, 15);
  var SpeedRatio = initSpeed;
  var speedValue = 1;
  var angle = sliderAngle.value;

  var initSpeed = sliderSpeed.value;
  var speed = initSpeed;
  var initBallSpeed;

  var tombe = false;

  initBallSpeed = (2*massCue) / (massCue + massBall) * initSpeed + (massCue - massBall) / (massCue + massBall)
  console.log(initBallSpeed);
  function frame() {
      speedValue = initBallSpeed - 0.1*9.81*id*(1/initBallSpeed);
      SpeedRatio = initBallSpeed / speedValue;
      speed /= SpeedRatio;
      if(speed < 0.2){
        speed = 0;
      }
      posX += speed * Math.cos(angle*Math.PI/180) * id;
      posY += speed * Math.sin(angle*Math.PI/180) * id;

      if(posX > 1215 || posX < 10){
        angle = 180-angle;
      }
      if(posY > 580 || posY < 25){
        angle = 360 - angle;
      }
      elem.style.top = posY + 'px'; 
      elem.style.left = posX + 'px'; 
  }
}
